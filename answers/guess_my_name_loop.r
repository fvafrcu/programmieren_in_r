my_name  <- "Dominik Cullmann"
guess <- readline(prompt = "Guess my name: ")
# "! identical(my_name, guess)" is better than "my_name != guess" but harder to
# read, so I stick with the inferior "!=".
while (my_name != guess) {
    print("You lose!")
    guess <- readline(prompt = "Try again: ")
}
print("You win!")


# There's a second, shorter version, which cannot easily be adopted from the
# if-version:
my_name <- "Dominik"
while (readline("Rate meinen Namen: ") != my_name) {
  print("Try again!")
}
