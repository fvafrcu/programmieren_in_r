#  `%2$s` bedeutet: nimm das zweite Argument nach dem eigentlichen Text (2$) und setze
#  es hier (%) als Zeichenkette, auf Englisch: string (s), ein.
sprintf("Hallo %2$s, ich hoffe, Du hast gut geschlafen! Grüße, %1$s", "Dein Name", "Dominik")
