# Einleitung

## Herzlich Willkommen, sch&ouml;n, dass ihr da seid!
Ich heisse Dominik Cullmann, ihr seid hier bei **Programmieren in R**

## Will euch als Gruppe kenenlernen.
Aufstellen nach

- Wissenschaftliche, Andere (nominal)
- Vorkenntnisse (metrisch)
    - Keine Zeile geschrieben
    - Ja, schon mal
    - Ja, aber darf keiner sehen.
    - Klar, Publikation samt Code. 

## Vorstellungsrunde
- Name
- Arbeitsgebiet inhaltlich (Versuchfl&auml;che, forstliche Ausbildung?)
- Warum seid ihr hier?


## Wieso, Weshalb, Warum?
  - Programmieren?  
    Bessere Wissenschaft
  - R?  
    Wird viel genutzt, auch von mir.
    R ist eine **auch** Programmiersprache!
  - Ich?  
    Weil's niemand anders macht.
    Ich bin nicht allein: Elke, Benutzerservice.


## Meine Ziele  
-  Ein Paar von euch anstecken. Vier ist okay, alle ist super.
- Keine Inhaltlichen Ziele

## Meine Anforderungen
  Einstiegskurs. Ein paar vielleicht &uuml;berqualifiziert.

## Worum geht's und worum nicht?
  Programmieren. Ihr werdet R hinterher nicht koennen, aber vielleicht koennen
  wollen. Es geht nicht um Statistik oder Modelle


## Zur Form
- Duzen
- Pausen einfordern
- Trinken
- Beschweren

## Pausenspiele
- Aufstellung nach Geburtsort, zwei Personen w&auml;hlen, durch langsames
  Gehen ein gleichschenkliges Dreieck bilden. 
  Gleichgewicht.
- Aufstellung nach letztem Urlaubsziel, zwei Personen w&auml;hlen, durch langsames
  Gehen so oft wie m&ouml;glich zwischen diesen Beiden hindurchgehen. 
  Tempo erh&ouml;hen
- Paare bilden, je ein Monster, eine Prinzessin. Prinzessinen fliehen theatralisch
  kreischend, Monster verfolgt sie. Bei Fang Rollenwechsel.
