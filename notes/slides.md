## Programmieren in R
* Vorstellungsrunde
* Wieso, Weshalb, Warum?
* Meine Ziele
* Meine Anforderungen
* Worum geht's und worum nicht?
* Zur Form
* Technische Vorbereitung

## Inhalte
* Grundlagen
* Kontrollstrukturen
* R und das Dateisystem
* Daten
* Funktionen
* Von Daten zur Graphik

##  Inhalt I
### Grundlagen
    * R benutzen
    * Hilfe finden mit der Hilfefunktion
    * Zuweisungen
    * Operatoren

### Kontrollstrukturen
    * Bedingte Anweisungen
    * Schleifen 
##  Inhalt II
### R und das Dateisystem
### Daten
    * Daten lesen und schreiben
    * Datentypen und Indizierung
### Funktionen
### Von Daten zur Graphik

## Abschluss
* Programmieren und Fehler
* R User Group und Mailingliste
* Programmierleitfaden
* Fragebogen
* Danke
