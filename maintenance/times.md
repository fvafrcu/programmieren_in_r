# Vorweg
5
## **R** -- Was ist denn R?
5
## Diese Einführung -- Was erhofft ihr euch?
5
###  **R**-Code und Output
0
### Aufgaben
0
# Grundlagen
## **R** benutzen -- RStudio Vorfuehrung
60
### R im Batch benutzen
0
## Hilfe finden mit der Hilfefunktion
5
## Zuweisungen -- Speicher 
10
## Operatoren
10
# Kontrollstrukturen
2
## Bedingte Anweisungen
0
### If-else
60
### `switch()`
30
## Schleifen 
5
### Gute Schleifen
45
### `apply()`-"Schleifen"
5
### Schlechte Schleifen
30
# **R** und das Dateisystem
30
## **R** im Batch
20
# Daten
10
## Daten lesen und schreiben
### Textdateien
5
### Microsoft-Access-Datenbanken
5
### Microsoft-Excel-Arbeitsmappen
5
## Datentypen und Indizierung
10
### data.frames
5
#### Numerischer Index
5
#### Namensindex
5
#### Logischer Index
5
#### Bereiche
5
#### Weitere Formen
5
### `list`s
5
### `vector`s
10
### `factor`s
15
## Mehr von mir zu Datentypen 
0
# Funktionen
60
# Von Daten zur Graphik
60
# Hintan
