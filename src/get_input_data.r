probably_at_fva <- function() {
    at_fva <- grepl("FVAFR-",  Sys.info()["nodename"]) || 
    grepl("fvafr",  Sys.info()["nodename"]) ||
    "f060" == Sys.info()["nodename"]
    return(at_fva)
}

create_local_dir <- function() {
    not_so_temporary_directory <- file.path(dirname(tempdir()) , "R_intro")
    dir.create(not_so_temporary_directory, showWarnings = FALSE)
    input_directory <- file.path(not_so_temporary_directory, "data", "input")
    unlink(input_directory, recursive = TRUE)
    dir.create(input_directory, recursive = TRUE)
    return(input_directory)
}

install_requirements <- function(requirements) {
    for (package in requirements) {
        if (! require(package, character.only = TRUE)) {
            install.packages(package)
        }
    }
}

input_url <- "https://fvafrcu.gitlab.io/programmieren_in_r/data/input/"

input_directory <- create_local_dir()
install_requirements("httr")

if (probably_at_fva()) {
    content <- httr::GET(input_url,
                         httr::use_proxy("10.127.255.17", 8080))
} else {
    content <- httr::GET(input_url)
}

hrefs <- grep("/", 
              grep("href", 
                   strsplit(as.character(content), "<")[[1]]
                   , value = TRUE
                   )
              , value = TRUE, invert = TRUE
              )
files  <- sapply( strsplit(hrefs, "\\\"") ,"[[", 2)  
for (file in files) {
    file_url <- file.path(input_url, file)
    file_path <- file.path(input_directory, file)
    if (Sys.info()["sysname"] == "Windows") {
        download.file(file_url, file_path, method = "wininet", mode = "wb")  
    } else {
        download.file(file_url, file_path, method = "wget")  
    }
}
