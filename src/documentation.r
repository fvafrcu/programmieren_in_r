#!/usr/bin/Rscript --vanilla
library("documentation")
create_markdown_documentation("graphics.r", arguments = "--no-header")
