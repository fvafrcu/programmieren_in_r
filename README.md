This is an introduction to programming in R written for people working at 
Forest Research Institute of Baden-Wuerttemberg.

It's not a general introduction to programming in R or programming in general, 
it's rather an internal document.

Go to [https://fvafrcu.github.io/programmieren_in_r](https://fvafrcu.github.io/programmieren_in_r).

Feel free to fork it crediting the LICENSE, but do not expect me to pull.

